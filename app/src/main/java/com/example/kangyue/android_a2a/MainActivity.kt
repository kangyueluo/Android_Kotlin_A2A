package com.example.kangyue.android_a2a

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    private var btnGoToPage2 : Button? = null
    private var btnGoToPage3 : Button? = null
    private var tvMsg :TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnGoToPage2 = findViewById(R.id.btn_go_to_page2)
        btnGoToPage3 = findViewById(R.id.btn_go_to_page3)
        tvMsg = findViewById(R.id.tv_msg)

        btnGoToPage2!!.setOnClickListener(){
            goToPage2()
        }

        btnGoToPage3!!.setOnClickListener(){
            goToPage3()
        }

    }

    private fun goToPage2(){
       val it2Page2  = Intent()
        it2Page2.setClass(this,Page2::class.java)
        startActivity(it2Page2)
    }

    private fun goToPage3(){
        val it2Page3 = Intent()
        it2Page3.setClass(this,Page3::class.java)
        startActivity(it2Page3)

        finish()
    }
}
