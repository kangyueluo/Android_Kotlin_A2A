package com.example.kangyue.android_a2a

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class Page2 : AppCompatActivity() {

    private var  btnJmpToPage3 :Button? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_page2)

        btnJmpToPage3 = findViewById(R.id.btn_jump_to_page3)
        btnJmpToPage3!!.setOnClickListener {
            jumpToPage3()
        }
    }

    private fun jumpToPage3(){
        val itjump2Page3 = Intent()
        itjump2Page3.setClass(this,Page3 ::class.java)
        startActivity(itjump2Page3)

        finish()

    }

}
